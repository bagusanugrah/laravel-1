<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SanberBookController extends Controller
{
    public function index(){
        return view('index');
    }

    public function register(){
        return view('register');
    }

    public function berhasil(Request $request){
        $first = $request['nama_depan'];
        $last = $request['nama_belakang'];
        return view('sukses',['nama_depan'=>$first, 'nama_belakang'=>$last]);
    }
}
