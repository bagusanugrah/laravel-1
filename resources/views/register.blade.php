<!DOCTYPE html>
<html>
    <head>
        <title>Form</title>
    </head>
    <body>
        <h1>Buat Account Baru!</h1>
        <h3>Sign Up Form</h3>
        <form action="/welcome" method="POST">
            @csrf
            <label for="first_name">First name:</label><br><br>
            <input type="text" id="first_name" name="nama_depan">
            <br>
            <br>
            <label for="last_name">Last name:</label><br><br>
            <input type="text" id="last_name" name="nama_belakang">
            <br>
            <br>
            <label>Gender:</label><br><br>
            <input type="radio" name="gender" id="male" value="male"><label for="male"> Male</label><br>
            <input type="radio" name="gender" id="female" value="female"><label for="female"> Female</label><br>
            <input type="radio" name="gender" id="other" value="other"><label for="other"> Other</label>
            <br>
            <br>
            <label>Nationaity:</label><br><br>
            <select>
                <option value="i">Indonesian</option>
                <option value="s">Singapoeran</option>
                <option value="m">Malaysian</option>
                <option value="a">Australian</option>
            </select>
            <br>
            <br>
            <label>Language Spoken:</label><br><br>
            <input type="checkbox" name="lang" id="indo" value="0"><label for="indo"> Bahasa Indonesia</label><br>
            <input type="checkbox" name="lang" id="eng" value="1"><label for="eng"> English</label><br>
            <input type="checkbox" name="lang" id="o" value="2"><label for="o"> Other</label>
            <br>
            <br>
            <label for="bio">Bio:</label><br><br>
            <textarea id="bio" cols="30" rows="10"></textarea>
            <br>
            <input type="submit" value="Sign Up">
        </form>
    </body>
</html>